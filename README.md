# Rust Serverless Transformer Endpoint
This project aims to develop and deploy a LLM to a serverless AWS Lambda function, using Rust and Cargo Lambda, and to set up a local LLM inference endpoint.

## Goals
* Dockerize Hugging Face Rust transformer
* Deploy container to AWS Lambda
* Implement query endpoint

## Steps
### Step 0: LLM Model Selection
Due to the nature of the AWS Lambda function that its process is automatically terminated after 15 minutes, we are unable to implement too large model. Thus, among the 11 LLM models listed in the `Hugging Face: rustformers - Large Language Models in Rust` repo, `rustformers/pythia-ggml` is chosen since it is a relatively small LLM and is more suitable with AWS Lambda function and the practice of this project.

### Step 1: Setup
1.  Initializing new AWS Lambda project in Rust using command line `cargo lambda new <PROJECT_NAME>` in terminal.
2. Add necessary dependencies to `Cargo.toml` file.
3. Add functional implementations and inference endpoint in `main.rs` file.
4. Then, you can run `cargo lambda watch`, and to test locally using the following commands in terminal:
```
curl http://localhost:9000/default/<transformer>\?text="Faker is Jaxn because"
```

### Step 2: Dockerization and Elastic Container Registry (ECR)
1. If the inference endpoint works locally, then we can navigate into the `Identity and Access Management (IAM)` under AWS console. 
2. Add a new user and attach necessary polices including 
* `IAMFullAccess`
* `AWSLambda_FullAccess`
* `AWSAppRunnerServicePolicyForECRAccess`
* `EC2InstanceProfileForImageBuilderECRContainerBuilds`
3. Naviagte into `ECR` under AWS console and create a new private repository.
4. Associates our local Docker with AWS through
```
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <AWS-ACCOUNT-NUMBER>.dkr.ecr.us-east-1.amazonaws.com

```
5. Then, build the docker image by
```
docker buildx build --progress=plain --platform linux/arm64 -t transformer .
```
6. Then, tag the image by
```
docker tag transformer:latest 590183895316.dkr.ecr.us-east-1.amazonaws.com/mini10:latest
```
7. Finally, push the image to the ECR repository we just created by
```
docker push 590183895316.dkr.ecr.us-east-1.amazonaws.com/mini10:latest
```

### Step 3: AWS Lambda
1. After sucessfuly pushing the docker image to ECR, navigate into `Lambda` under AWS console. 
2. Create a new function with the option `Container image`, enter the Amazon ECR image URL, and choose `arm64` architecture.
3. Then, click into your lambda function, and under `configuration`, navigate into the `General configuration`, and adjust the Memory and timeout setting based on the characteristics of your LLM (token etc.).
4. Then, click into the `functional URL`, create a new function URL with `CORS enabled`.

## Results
### Testing locally
![local_1](https://gitlab.com/dukeaiml/IDS721/transformer/-/wikis/uploads/676ffa7b21d5499a06354e3384869e4c/local_1.png)
![local_2](https://gitlab.com/dukeaiml/IDS721/transformer/-/wikis/uploads/e2d43919e8749e46fdd4dd82920754e3/local_2.png)

### ECR
![ECR](https://gitlab.com/dukeaiml/IDS721/transformer/-/wikis/uploads/68cacba028334d1e1466665eed857a3c/ecr.png)

### AWS Lambda
![local_2](https://gitlab.com/dukeaiml/IDS721/transformer/-/wikis/uploads/31e26f7868eb8f7fd54bd6753764a095/aws_lambda.png)

### cURL Request against Endpoint
You can use both of these request
```
curl -X POST https://pkbahxwst2kgb56bqq2imkzjem0epsto.lambda-url.us-east-1.on.aws/ -H "Content-Type: application/json" -d '{"text":"Faker is Jaxn because"}'
```
or
```
Curl https://pkbahxwst2kgb56bqq2imkzjem0epsto.lambda-url.us-east-1.on.aws/\?text="Faker is Jaxn because"
```
NB: It takes time to run, be patient!
![aws_test](https://gitlab.com/dukeaiml/IDS721/transformer/-/wikis/uploads/69315a7578e47096466d376b2d0c6fc4/aws_test.png)

### Lambda Log showing Successful Invoke and Deployment
![cloud_watch](https://gitlab.com/dukeaiml/IDS721/transformer/-/wikis/uploads/adfb701e1103ea91d184a062f52aa036/cloud_watch.png)



